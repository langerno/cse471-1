#include "pch.h"
#include "CReverb.h"

CReverb::CReverb(double wet, double dry) : CEffect(wet, dry)
{
	m_lbuffer.resize(DELAY, 0.0);
	m_rbuffer.resize(DELAY, 0.0);
	m_index = 0;
}

void CReverb::apply(const double* input, double* output)
{

    // Add output of the queue to the current input
    output[0] = input[0] + m_lbuffer.at(m_index);
	output[1] = input[1] + m_rbuffer.at(m_index);

	m_lbuffer.at(m_index) = input[0] + (m_lbuffer.at(m_index) * DECAY);
	m_rbuffer.at(m_index) = input[1] + (m_rbuffer.at(m_index) * DECAY);

	m_index = (m_index + 1) % DELAY;

}

