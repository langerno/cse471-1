#pragma once
#include "CEffect.h"

const double THRESHOLD = 0.7;
const double RATIO = 2.0;

class CCompression :
    public CEffect
{
public:
    CCompression() : CEffect() {}
    CCompression(double wet, double dry) : CEffect(wet, dry) {}
    void apply(const double* input, double* output) override;

};

