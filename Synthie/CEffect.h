#pragma once
class CEffect
{
	double m_wet;
	double m_dry;

public:

	CEffect() { m_wet = 1; m_dry = 1; }

	CEffect(double wet, double dry) : m_wet(wet), m_dry(dry) {}

	~CEffect() {}

	virtual void apply(const double* input, double* output) = 0;

};

