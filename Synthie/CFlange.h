#pragma once
#include "CEffect.h"

#include <vector>

const double MODULATION_FREQUENCY = 0.01;
const double MODULATION_DEPTH = 0.5;
const int MAX_DELAY_FRAMES = 1000;

class CFlange :
    public CEffect
{
private:
    std::vector<double> m_lbuffer;
    std::vector<double> m_rbuffer;
    double m_phase = 0;
    double m_sampleRate;
    int m_index = 0;
    int m_bufferHead = 0;
    int m_sign = 1;
public:
    CFlange() : CEffect() {}
    CFlange(double wet, double dry, double sampleRate);
    void apply(const double* input, double* output) override;
};

