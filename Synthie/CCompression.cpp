#include "pch.h"
#include "CCompression.h"
#include <cmath>

void CCompression::apply(const double* input, double* output)
{
    if (std::abs(input[0]) > THRESHOLD) {
        output[0] = THRESHOLD + (std::abs(input[0]) - THRESHOLD) / RATIO;
    }
    else {
        output[0] = input[0];
    }

    if (std::abs(input[1]) > THRESHOLD) {
        output[1] = THRESHOLD + (std::abs(input[1]) - THRESHOLD) / RATIO;
    }
    else {
        output[1] = input[1];
    }
}
