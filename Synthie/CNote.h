#pragma once

#include <string>
#include <map>
#include <utility>
#include <vector>

#include "xmlhelp.h"

using InstrumentConfig = std::vector<std::pair<std::wstring, std::wstring>>;

class CNote
{
public:
    CNote(void);
    virtual ~CNote(void);

    int Measure() const { return m_measure; }
    double Beat() const { return m_beat; }
    InstrumentConfig GetInstrumentConfig() const { return m_instrumentConfig; }
    const std::wstring& Instrument() const { return m_instrument; }
    IXMLDOMNode* Node() { return m_node; }

private:
    std::wstring m_instrument;
    int m_measure;
    double m_beat;
    InstrumentConfig m_instrumentConfig;
    CComPtr<IXMLDOMNode> m_node;
public:
    void XmlLoad(IXMLDOMNode* xml, std::wstring& instrument, InstrumentConfig &instrumentConfig);
    bool operator<(const CNote& b);
};
