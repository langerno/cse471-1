#include "pch.h"
#include "CRing.h"

CRing::CRing(double wet, double dry, double sampleRate) : CEffect(wet, dry), m_sampleRate(sampleRate)
{
}

void CRing::apply(const double* input, double* output)
{
    double modulatingSignal = AMPLITUDE * sin(2.0 * PI * FREQUENCY * m_sampleNum / SAMPLE_RATE);

    // Apply ring modulation by multiplying input sample with modulating signal
    output[0] = input[0] * modulatingSignal;
    output[1] = input[1] * modulatingSignal;

    m_sampleNum++;
}
