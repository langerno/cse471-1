#include "pch.h"
#include "CSubtractiveSynth.h"
#include "Notes.h"
#include "audio/Wave.h"

const double LOOP_FRAME_RAMP = 100.0;

CSubtractiveSynth::CSubtractiveSynth(InstrumentConfig instrumentConfig) : CInstrument(instrumentConfig)
{
	//
	// Reson default Parameters
	//
	m_reson = false;
	m_resonFrequency = 0.00998; 
	m_resonBandwidth = 0.015;
	m_resonGain = 1;


	//
	// Reson Frequency ASDR default parameters
	//
	m_resonBandASDR = false;
	m_bandAttack = 0;
	m_bandSustain = 1;
	m_bandDecay = 0;
	m_bandRelease = 0;

	//
	// Reson Bandwidth ASDR default parameters
	//
	m_resonFreqASDR = false;
	m_freqAttack = 0;
	m_freqSustain = 1;
	m_freqDecay = 0;
	m_freqRelease = 0;


	//
	// Attack/Release default parameters
	//
	m_attackRelease = false;
	m_attack = 0;
	m_release = 0;
	m_sustain = 1;
	m_decay = 0;


	//
	// Wave Type default
	//
	m_waveType = triangle;


	//
	// Parse custom config data
	//
	for (std::pair<std::wstring, std::wstring> configItem : instrumentConfig)
	{
		std::wstring key = configItem.first;
		std::wstring value = configItem.second;
		//
		// Reson Filter settings
		//
		if (key == L"resonSettings")
		{
			m_reson = true;
			std::wstring list = value;
			int prev = 0;
			int vals = 0;
			for (int i = prev; i < list.size(); i++)
			{
				if (list[i] == ',')
				{
					switch (vals) {
					case 0:
						m_resonFrequency = stod(list.substr(0, i));
						break;
					case 1:
						m_resonBandwidth = stod(list.substr(prev, i));
						break;
					}
					prev = i + 1;
					vals++;
				}
			}
			m_resonGain = stod(list.substr(prev, list.size() - prev));
		}
		// Reson Bandwidth ASDR settings
		else if (key == L"resonBandwidthASDR")
		{
			m_resonBandASDR = true;
			std::wstring list = value;
			int prev = 0;
			int vals = 0;
			for (int i = prev; i < list.size(); i++)
			{
				if (list[i] == ',')
				{
					switch (vals) {
					case 0:
						m_bandAttack = stod(list.substr(0, i));
						break;
					case 1:
						m_bandSustain = stod(list.substr(prev, i));
						break;
					case 2:
						m_bandDecay = stod(list.substr(prev, i));
						break;
					}
					prev = i + 1;
					vals++;
				}
			}
			m_bandRelease = stod(list.substr(prev, list.size() - prev));

		}
		// Reson Frequency ASDR settings
		else if (key == L"resonFrequencyASDR")
		{
			m_resonFreqASDR = true;
			std::wstring list = value;
			int prev = 0;
			int vals = 0;
			for (int i = prev; i < list.size(); i++)
			{
				if (list[i] == ',')
				{
					switch (vals) 
					{
						case 0:
							m_freqAttack = stod(list.substr(0, i));
							break;
						case 1:
							m_freqSustain = stod(list.substr(prev, i));
							break;
						case 2:
							m_freqDecay = stod(list.substr(prev, i));
							break;
					}
					prev = i + 1;
					vals++;
				}
			}
			m_freqRelease = stod(list.substr(prev, list.size() - prev));
		}
		//
		// Amplitude ASDR Envelope settings
		//
		else if (key == L"ASDRSettings")
		{
			m_attackRelease = true;
			std::wstring list = value;
			int prev = 0;
			int vals = 0;
			for (int i = prev; i < list.size(); i++)
			{
				if (list[i] == ',')
				{
					switch (vals) {
						case 0:
							m_attack = stod(list.substr(0, i));
							break;
						case 1:
							m_sustain = stod(list.substr(prev, i));
							break;
						case 2:
							m_decay = stod(list.substr(prev, i));
							break;
					}
					prev = i + 1;
					vals++;
				}
			}
			m_release = stod(list.substr(prev, list.size() - prev));
		}
		//
		// Wave type
		//
		else if (key == L"wave")
		{
			if (value == L"square") { m_waveType = square; }
			else if (value == L"triangle") { m_waveType = triangle; }
			else if (value == L"sawtooth") { m_waveType = sawtooth; }
		}
	}
}

CSubtractiveSynth::CSubtractiveSynth(boolean reson = true, double resonFreq = 0.00998, double resonGain = 10.5, double resonBand = 0.015,
									boolean attackRelease = true, double m_release = 0.05, double m_attack = 0.05)
{
	//
	// Reson Parameters
	//
	m_reson = reson;
	m_resonFrequency = resonFreq;
	m_resonBandwidth = resonBand;
	m_resonGain = resonGain;
	// Reson intermediate values
	m_R = 1 - (m_resonBandwidth / 2);
	m_cos_theta = 2 * m_R * cos(2 * PI * m_resonFrequency) / (1 + pow(m_R, 2));
	m_A = ((1 - pow(m_R, 2)) * sqrt((1 - pow(m_cos_theta, 2)))) * m_resonGain;

	//
	// Attack/Release parameters
	//
	m_attackRelease = attackRelease;
	m_release = m_release;
	m_attack = m_attack;
}

void CSubtractiveSynth::Start()
{
	m_ind = 0;
	m_time = 0;

	m_latterFrame[0] = 0;
	m_latterFrame[1] = 0;

	m_formerFrame[0] = 0;
	m_formerFrame[1] = 0;


}

void CSubtractiveSynth::applyReson(double* input, double* output) 
{

	double beat_number = (GetBPM() / 60) * m_time;

	double bandwidth = m_resonBandwidth;
	double frequency = m_resonFrequency;

	// Just reusing ASDR calculator for frames;
	if (m_resonBandASDR)
	{
		bandwidth = agnosticASDR(m_resonBandwidth, beat_number, m_duration, m_bandAttack, m_bandSustain, m_bandDecay, m_bandRelease);
	}
	if (m_resonFreqASDR)
	{
		frequency = agnosticASDR(m_resonFrequency, beat_number, m_duration, m_freqAttack, m_freqSustain, m_freqDecay, m_freqRelease);
	}


	m_R = 1 - (bandwidth / 2);
	m_cos_theta = 2 * m_R * cos(2 * PI * frequency) / (1 + pow(m_R, 2));
	m_A = ((1 - pow(m_R, 2)) * sqrt((1 - pow(m_cos_theta, 2)))) * m_resonGain;

	// Calculate reson for both sides
	double sampleL = (m_A * input[0]) + (( 2 * m_R * m_cos_theta ) * m_latterFrame[0]) - ( pow(m_R, 2) * m_formerFrame[0] );
	double sampleR = (m_A * input[1]) + (( 2 * m_R * m_cos_theta ) * m_latterFrame[1]) - ( pow(m_R, 2) * m_formerFrame[1] );

	output[0] = sampleL;
	output[1] = sampleR;

	m_formerFrame[0] = m_latterFrame[0];
	m_formerFrame[1] = m_latterFrame[1];

	m_latterFrame[0] = sampleL;
	m_latterFrame[1] = sampleR;
}

double CSubtractiveSynth::agnosticASDR(double input, double beatnum, double duration,
	double attack, double sustain, double decay, double release)
{
	double ramp = 1;
	double release_start = (duration - release);
	double decay_end = (attack + decay);

	if (beatnum < attack)
	{
		ramp = beatnum / attack;
	}
	else if (beatnum > release_start)
	{
		ramp = sustain - ((beatnum - release_start) / release) * sustain;
	}
	else if (beatnum > attack && beatnum < decay_end)
	{
		ramp = sustain + (1 - sustain) * (1 - (beatnum / decay_end));
	}
	else
	{
		ramp = sustain;
	}

	return input * ramp;
}

void CSubtractiveSynth::applyASDR(double* input, double* output, double beatnum)
{
	output[0] = agnosticASDR(input[0], beatnum, m_duration, m_attack, m_sustain, m_decay, m_release);
	output[1] = agnosticASDR(input[1], beatnum, m_duration, m_attack, m_sustain, m_decay, m_release);
}

bool CSubtractiveSynth::Generate()
{
	double beat_number = (GetBPM() / 60) * m_time;


	if(beat_number < m_duration)
	{
		double wave_frame[2];
		wave_frame[0] = wave_frame[1] = m_noteWave[m_ind];

		// Ramp to make sure no clicking when wave audio loops
		double ramp = 1;
		int noteSampleSize = m_noteWave.size();
		int transitionStart = noteSampleSize - LOOP_FRAME_RAMP;
		if(m_ind > transitionStart)
		{
			ramp = 1.0 - ((m_ind - transitionStart)/ LOOP_FRAME_RAMP);

		}
		wave_frame[0] *= ramp;
		wave_frame[1] *= ramp;
	
		// If Reson is on, apply effects
		if(m_reson)
		{
			double filtered_frame[2];
			applyReson(wave_frame, filtered_frame);
			wave_frame[0] = filtered_frame[0];
			wave_frame[1] = filtered_frame[1];
		}

		// If Amplitude ASDR is on, apply effects
		if (m_attackRelease)
		{
			double filtered_frame[2];
			applyASDR(wave_frame, filtered_frame, beat_number);
			wave_frame[0] = filtered_frame[0];
			wave_frame[1] = filtered_frame[1];
		}

		m_frame[0] = wave_frame[0];
		m_frame[1] = wave_frame[1];

		m_ind = (m_ind + 1) % noteSampleSize;
		m_time += GetSamplePeriod();
		return true;
	}
	else {
		return false;
	}
}

void CSubtractiveSynth::SetNote(CNote* note)
{
	// Get a list of all attribute nodes and the
   // length of that list
	CComPtr<IXMLDOMNamedNodeMap> attributes;
	note->Node()->get_attributes(&attributes);
	long len;
	attributes->get_length(&len);

	// Loop over the list of attributes
	for (int i = 0; i < len; i++)
	{
		// Get attribute i
		CComPtr<IXMLDOMNode> attrib;
		attributes->get_item(i, &attrib);

		// Get the name of the attribute
		CComBSTR name;
		attrib->get_nodeName(&name);

		// Get the value of the attribute.  A CComVariant is a variable
		// that can have any type. It loads the attribute value as a
		// string (UNICODE), but we can then change it to an integer 
		// (VT_I4) or double (VT_R8) using the ChangeType function 
		// and then read its integer or double value from a member variable.
		CComVariant value;
		attrib->get_nodeValue(&value);

		if (name == "duration")
		{
			value.ChangeType(VT_R8);
			SetDuration(value.dblVal);
		}
		else if (name == "note")
		{
			std::wstring folder;
			switch (m_waveType) {
				case triangle:
					folder = L"res/triangle/";
					break;
				case sawtooth:
					folder = L"res/sawtooth/";
					break;
				case square:
					folder = L"res/square/";
					break;
			}
			m_note = (value.bstrVal);
			std::wstring path = folder + m_note + L".wav";
			CWaveIn load_wave;
			load_wave.open(path.c_str());
			short frame[2];
			while(load_wave.ReadFrame(frame))
			{
				m_noteWave.push_back((frame[0]*0.00003));
			}
		}
	}
}
