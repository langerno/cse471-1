#include "pch.h"
#include "CInstrument.h"

CInstrument::CInstrument(InstrumentConfig config)
{
	setEffectSend(0, 1);
	for (int c = 1; c < 5; c++)
	{
		setEffectSend(c, 0);
	}

	for (std::pair<std::wstring, std::wstring> configItem : config)
	{
		std::wstring key = configItem.first;
		std::wstring value = configItem.second;
		//
		// Effects sends for this instrument
		//
		if (key == L"effectDist")
		{
		std::wstring list = value;
		int prev = 0;
		int vals = 0;
		for (int i = prev; i < list.size(); i++)
		{
			if (list[i] == ',')
			{
				switch (vals) {
				case 0:
					setEffectSend(vals, stod(list.substr(0, i)));
					break;
				case 1:
					setEffectSend(vals, stod(list.substr(prev, i)));
					break;
				case 2:
					setEffectSend(vals, stod(list.substr(prev, i)));
					break;
				case 3:
					setEffectSend(vals, stod(list.substr(prev, i)));
					break;
				}
				prev = i + 1;
				vals++;
			}
		}
		setEffectSend(vals, stod(list.substr(prev, list.size() - prev)));
		}
	}
}
