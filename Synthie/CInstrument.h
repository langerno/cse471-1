#pragma once
#include "CAudioNode.h"
#include "CNote.h"
class CInstrument :
    public CAudioNode
{
    double m_effectSends[5];
    double frame = 0;
public:
    virtual void SetNote(CNote* note) = 0;
    CInstrument() {}
    CInstrument(InstrumentConfig config);
    void setEffectSend(int effect, double ratio) { m_effectSends[effect] = ratio; }
    double getWetRatio(int effect) { return m_effectSends[effect]; }
};

