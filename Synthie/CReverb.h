#pragma once
#include "CEffect.h"

#include <vector>

const int DELAY = 10000;
const double DECAY = 0.25;

class CReverb :
    public CEffect
{
    int m_index = 0;
    std::vector<double> m_lbuffer;
    std::vector<double> m_rbuffer;

public:

    CReverb() : CEffect() {}

    CReverb(double wet, double dry);

    void apply(const double* input, double* output) override;

};

