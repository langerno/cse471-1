#include "pch.h"
#include "AR.h"

CAR::CAR()
{
}

void CAR::Start()
{
    m_time = 0;
}

bool CAR::Generate()
{
     double bps = (GetBPM() / SECONDS_IN_MINUTE);
     double beats_occurred = bps * m_time;

    // Tell the component to generate an audio sample
    m_source->Generate();

    double ramp = 1;
    double release_start = (m_duration - m_release);

    if (beats_occurred < m_attack)
    {
        ramp = beats_occurred / m_attack;
    }
    else if (beats_occurred > release_start)
    {
        ramp = 1.0 - (beats_occurred - release_start) / m_release;
    }

    // Read the component's sample and make it our resulting frame.
    m_frame[0] = ramp * (m_source->Frame(0));
    m_frame[1] = ramp * (m_source->Frame(1));

    // Update time
    m_time += GetSamplePeriod();

    // We return true until the time reaches the duration.
    return beats_occurred < m_duration;
}
