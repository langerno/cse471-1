#pragma once
#include "CEffect.h"

const double AMPLITUDE = 0.5;
const double FREQUENCY = 200;
const int SAMPLE_RATE = 44100;

class CRing :
    public CEffect
{
    int m_sampleNum = 0;
    double m_sampleRate;
public:
    CRing() : CEffect() {}
    CRing(double wet, double dry, double m_sampleRate);
    void apply(const double* input, double* output) override;
};

