#pragma once
#include "CInstrument.h"
#include "AR.h"

#include <vector>
#include <string>
#include <map>
#include <utility>

using wavetable = std::map<std::string, std::vector<double>>;

using InstrumentConfig = std::vector<std::pair<std::wstring, std::wstring>>;

const enum WAVE_TYPE{sawtooth, triangle, square};

class CSubtractiveSynth :
    public CInstrument
{
private:
    int m_ind = 0;
    double m_duration;
    double m_time;
    std::wstring m_note;
    std::vector<double> m_noteWave;
    WAVE_TYPE m_waveType = square;

    //
    // Reson Parameters
    //
    boolean m_reson;
    double m_resonFrequency;
    double m_resonBandwidth;
    double m_resonGain;
    // Reson intermediate values
    double m_formerFrame[2];
    double m_latterFrame[2];
    double m_R;
    double m_A;
    double m_cos_theta;

    //
    // ASDR parameters
    //
    boolean m_attackRelease;
    double m_release;
    double m_attack;
    double m_sustain;
    double m_decay;

    //
    // Reson Bandwidth ASDR
    //
    boolean m_resonBandASDR;
    double m_bandRelease;
    double m_bandAttack;
    double m_bandSustain;
    double m_bandDecay;

    //
    // Reson Frequency ASDR
    //
    boolean m_resonFreqASDR;
    double m_freqRelease;
    double m_freqAttack;
    double m_freqSustain;
    double m_freqDecay;

    double CSubtractiveSynth::agnosticASDR(double input, double beatnum, double duration,
        double attack, double sustain, double decay, double release);



public:
    CSubtractiveSynth(InstrumentConfig instrumentConfig);
    CSubtractiveSynth(boolean reson, double resonFreq, double resonGain, double resonBand,
        boolean attackRelease, double m_release, double m_attack);
    void Start() override;
    bool Generate() override;
    void SetNote(CNote* note) override;
    void applyReson(double* input, double* output);
    void applyASDR(double* input, double* output, double beatnum);
    void SetDuration(double duration) { m_duration = duration; }
    void SetAttack(double a) { m_attack = (GetBPM() / 60) * a; }
    void SetRelease(double r) { m_release = (GetBPM() / 60) * r; }
};

