#pragma once
#include "CAudioNode.h"

const double SECONDS_IN_MINUTE = 60.0;

class CAR :
    public CAudioNode
{
    double m_attack;
    double m_release;
    double m_duration;
    double m_time;
    CAudioNode* m_source;
public:
    CAR();
    void SetDuration(double duration) { m_duration = duration; }
    void SetSource(CAudioNode* source) { m_source = source; }
    void SetAttack(double a) { m_attack = (GetBPM()/SECONDS_IN_MINUTE) * a; }
    void SetRelease(double r) { m_release = (GetBPM() / SECONDS_IN_MINUTE) * r; }
    virtual void Start();
    virtual bool Generate();
};

