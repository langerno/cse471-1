#pragma once
#include <list>
#include <string>
#include <vector>
#include <utility>
#include <map>

#include "msxml2.h"
#include "CInstrument.h"
#include "CNote.h"
#include "CReverb.h"
#include "CCompression.h"
#include "CFlange.h"
#include "CRing.h"
#include "CSubtractiveSynth.h"

class CSynthesizer
{

public:
	//! Number of audio channels
	int GetNumChannels() { return m_channels; }

	//! Sample rate in samples per second
	double GetSampleRate() { return m_sampleRate; }

	//! Sample period in seconds (1/samplerate)
	double GetSamplePeriod() { return m_samplePeriod; }

	//! Set the number of channels
	void SetNumChannels(int n) { m_channels = n; }

	//! Set the sample rate
	void SetSampleRate(double s) { m_sampleRate = s;  m_samplePeriod = 1.0 / s; }

	double GetBPM() { return m_bpm; }

	CSynthesizer();

private:
	double m_sampleRate;
	int m_channels;
	double m_samplePeriod;
	double  m_bpm;                  //!< Beats per minute
	int     m_beatspermeasure;  //!< Beats per measure
	double  m_secperbeat;        //!< Seconds per beat
	int m_currentNote;          //!< The current note we are playing
	int m_measure;              //!< The current measure
	double m_beat; //!< The current beat within the measure


	// Effects
	CReverb m_reverb;
	CCompression m_compression;
	CFlange m_flange;
	CRing m_ring;

public:
	// ! Start the synthesizer
	void Start();
	// ! Generate one audio frame
	bool Generate(double* frame);

	//! Get the time since we started generating audio
	double GetTime() { return m_time; }
private:
	double m_time;
	std::list<CInstrument*> m_instruments;
public:
	void Clear();
	void OpenScore(CString& filename);
private:
	void XmlLoadScore(IXMLDOMNode* xml);
	void XmlLoadInstrument(IXMLDOMNode* xml);
	void XmlLoadNote(IXMLDOMNode* xml, std::wstring& instrument, InstrumentConfig &instrumentConfig);
	std::vector<CNote> m_notes;
};

#pragma comment(lib, "msxml2.lib")


