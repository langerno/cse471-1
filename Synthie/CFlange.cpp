#include "pch.h"
#include "CFlange.h"

CFlange::CFlange(double wet, double dry, double sampleRate)
{
	m_lbuffer.resize(MAX_DELAY_FRAMES, 0);
	m_rbuffer.resize(MAX_DELAY_FRAMES, 0);
    m_sampleRate = sampleRate;
    m_phase = 0;
    m_bufferHead = MAX_DELAY_FRAMES - 1;
}

void CFlange::apply(const double* input, double* output)
{
    // Modulate the delay time using a low-frequency oscillator (sine wave)
    int totalDelay = MAX_DELAY_FRAMES * (( 1 + MODULATION_DEPTH ) * sin(2 * PI * MODULATION_FREQUENCY * m_phase));

    int readloc = (totalDelay + m_bufferHead) % MAX_DELAY_FRAMES;

    // Apply the flanger effect
    double ldelayedSample = m_lbuffer[readloc];
    output[0] = input[0] + ldelayedSample;

    double rdelayedSample = m_rbuffer[readloc];
    output[1] = input[1] + rdelayedSample;

    // Update the delay line
    m_lbuffer[m_bufferHead] = input[0];
    m_rbuffer[m_bufferHead] = input[1];


    m_phase += m_sign * (1 / m_sampleRate);
    if (m_phase > 1)
    {
        m_sign = -1;
    }
    else if (m_phase < 0)
    {
        m_sign = 1;
    }

    m_bufferHead = (m_bufferHead == 0) ? MAX_DELAY_FRAMES - 1 : m_bufferHead - 1;
}
