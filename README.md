# Techno Halo

### Group Members
- Nolan Langer

### Substractive Synthesis Component 
##### by Nolan Langer

The subtractive synthesis component works by taking specific wave types and *subtracting* from them via filters. The three waves at the disposal of the component are as follows
- Square
- Sawtooth
- Triangle


The waves can then be edited using the following tools with their associated variables

- Reson Filter
    - Adds just two frames prior multiplied by multiple intermediate values to each frame in the wave, can be modified using the following variables
      - Frequency
      - Bandwidth
      - Gain
- ASDR Envelopes
  - ASDR Envelopes can be applied to not only the amplitude of the wave but also to the frequency and bandwidth of the Reson filter. Each can use the following variables
    - Attack
      - Amount of time, in beats, the a note/reson takes to reach its peak value
    - Sustain
      - The level as a percentage of the peak value at which the note/reson maintains after the peak
    - Decay
      - How long, in beats, the note/reson takes to reach it's sustain percentage
    - Release
      - How long, in beats, the note/reson takes to fade to zero at the end of its duration

Finally, the component can play multiples notes at one time.

<details>
    <summary> Example Score Sheet Using Subtractive Synthesis Component (sub synth demo.score)</summary>

            <?xml version="1.0" encoding="utf-8"?>
            <score bpm="120" beatspermeasure="3">

            <!-- Square Wave -->
                <instrument instrument="SubtractiveSynth" wave="square">
                    <note measure="1" beat="1" duration="2" note="G3"/>
                </instrument>

            <!-- Sawtooth Wave -->
                <instrument instrument="SubtractiveSynth" wave="sawtooth">
                    <note measure="2" beat="1" duration="2" note="G3"/>
                </instrument>

            <!-- Triangle Wave -->
                <instrument instrument="SubtractiveSynth" wave="triangle">
                    <note measure="3" beat="1" duration="2" note="G3"/>
                </instrument>

            <!-- Reson Filter -->
                <instrument instrument="SubtractiveSynth" wave="square" resonSettings="0.0098,0.15,10.0">
                    <note measure="4" beat="1" duration="2" note="G3"/>
                </instrument>

            <!-- Reson Bandwidth Envelope Filter -->
                <instrument instrument="SubtractiveSynth" wave="square" resonSettings="0.03,0.15,15.0" resonBandwidthASDR="0.2,0.2,0.3,0.1">
                    <note measure="5" beat="1" duration="1" note="G3"/>
                <note measure="5" beat="2" duration="1" note="G3"/>
                </instrument>

            <!-- Reson Frequency Envelope Filter -->
                <instrument instrument="SubtractiveSynth" wave="square" resonSettings="0.03,0.15,10.0" resonFrequencyASDR="0.2,0.1,0.3,0.2">
                    <note measure="6" beat="1" duration="1" note="G3"/>
                    <note measure="6" beat="2" duration="1" note="G3"/>
                </instrument>

            <!-- Square Wave -->
                <instrument instrument="SubtractiveSynth" wave="square" ASDRSettings="0.1,0.75,0.2,0.2">
                    <note measure="7" beat="1" duration="1" note="G3"/>
                    <note measure="7" beat="2" duration="1" note="G3"/>
                </instrument>
            </score>
this example is stored in this repo as "sub synth demo.score" and can be downloaded by clicking [here](https://gitlab.msu.edu/langerno/cse471-1/-/raw/main/sub%20synth%20demo.score?ref_type=heads&inline=false)
</details>


### Effects Component 
##### by Nolan Langer

The project has four available effect boxes to choose from as well as the ability to control how much of the sound produced by an instrument is sent to which effect.
- Flange
  - A "jet-like" sound produced by modulating the period of a copy of the input sound and adding it to the input
- Reverb
  - A decaying "echo" effect created by adding incrementally quieting frames from earlier in the input to the current input
- Compression
  - Decreases the distance of highs and lows in the sound by reducing anything above a specific threshold by a specific ratio
- Ring Modulation w/ Sinusoid
  - Multiplies the input by a modulating sine wave

<details>
    <summary> Example Score Using Effects Component (effect demo.score)</summary>

    <?xml version="1.0" encoding="utf-8"?>
    <score bpm="120" beatspermeasure="5">

    <!-- Default, Dry -->
        <instrument instrument="SubtractiveSynth" wave="triangle" effectDist="1.0,0.0,0.0,0.0,0.0">
            <note measure="1" beat="1" duration="4" note="G3"/>
        </instrument>

    <!-- Flange -->
        <instrument instrument="SubtractiveSynth" wave="triangle" effectDist="0.0,1.0,0.0,0.0,0.0">
            <note measure="2" beat="1" duration="4" note="G3"/>
        </instrument>

    <!-- Reverb -->
        <instrument instrument="SubtractiveSynth" wave="triangle" effectDist="0.0,0.0,1.0,0.0,0.0">
            <note measure="3" beat="1" duration="1" note="G3"/>
        <note measure="3" beat="2" duration="1" note="A3"/>
        <note measure="3" beat="3" duration="1" note="B3"/>
        <note measure="3" beat="4" duration="1" note="C3"/>
        </instrument>

    <!-- Compression -->
        <instrument instrument="SubtractiveSynth" wave="triangle" effectDist="0.0,0.0,0.0,1.0,0.0">
            <note measure="4" beat="1" duration="4" note="G3"/>
        </instrument>

    <!-- Ring Modulation w/ Sinusoid -->
        <instrument instrument="SubtractiveSynth" wave="triangle" effectDist="0.0,0.0,0.0,0.0,1.0" >
            <note measure="5" beat="1" duration="1" note="G3"/>
        <note measure="5" beat="2" duration="1" note="A3"/>
        <note measure="5" beat="3" duration="1" note="B3"/>
        <note measure="5" beat="4" duration="1" note="C3"/>
        </instrument>

    <!-- Combination (Reverb + Flange) -->
        <instrument instrument="SubtractiveSynth" wave="triangle" effectDist="0.0,0.5,0.5,0.0,0.0" >
            <note measure="6" beat="1" duration="1" note="G3"/>
        <note measure="6" beat="2" duration="1" note="A3"/>
        <note measure="6" beat="3" duration="1" note="B3"/>
        <note measure="6" beat="4" duration="1" note="C3"/>
        </instrument>
    </score>

this is stored in this repo as "effect demo.score" and can be downloaded by clicking [here](https://gitlab.msu.edu/langerno/cse471-1/-/raw/main/effects%20demo.score?ref_type=heads&inline=false)
</details>

### Score File Format

The basic layout of the .score file is as follows

    <?xml version="1.0" encoding="utf-8"?>
    <score>
        <instrument>
            <note></note>
        </instrument>
    </score>

You can have as many instruments as you'd like in the score sheet, and as many notes as you'd like per instrument. Next is variables for each of the elements

- \<score\>
  - Required
    - bpm=
      - total beats per minute
    - beatspermeaure=
      - total beats in each measure
    - Example
                        
            <score bpm="120" beatspermeasure="4"></score>


- \<instrument\>
  - Required
    - instrument="instrument"
      - type of component to produce sound
        - SubtractiveSynth
          - Required
            - wave="triangle" (square, triangle, or sawtooth)
        - ToneInstrument
  - Optional
    - resonSettings="1.0,1.0,1.0"
      - sets the reson variables and turns on the filter, comma separated list of floats ordered as follows
        1. Frequency
        2. Bandwidth
        3. Gain
    - ASDR envelopes all ordered as Attack,Sustain,Decay,Release
      - resonBandwidthASDR="0.1,0.1,0.1,0.1"
        - sets the ASDR envelope for the reson bandwidth
      - resonFrequencyASDR="0.1,0.1,0.1,0.1"
        - sets the ASDR envelope for the reson frequency
      - ASDRSettings="1.0,1.0,1.0,1.0"
        - sets the ASDR envelope for the amplitude of the synth
    - effectsDist="1.0,0.0,0.0,0.0,0.0"
        - determines how much of the output of the instrument is sent to each effect box, also how much remains "dry". Works best when all values sum to 1.0, else is untested behavior. What each value represents is as follows:
            1. Dry
            2. Flange
            3. Reverb
            4. Compression
            5. Ring Modulation w/ Sinusoid
  - Example

            <instrument instrument="SubtractiveSynth" wave="square" resonSettings="0.0098,0.015,10.0" resonBandwidthASDR="0.1,0.5,0.2,0.1" resonFrequencyASDR="0.3,0.4,0.1,0.5" ASDRSettings="1.0,1.0,1.0,1.0" effectsDist="0.0,0.5,0.5,0.0,0.0"></instrument>

- \<note\>
  - Required
    - beat="1"
      - beat on which the note starts playing
    - measure="1"
      - measure in which the note plays
    - duration="1"
      - how long the note is played for
    - note="G3"
      - pitch that the note play 
  - Example

                <note measure="2" beat="1" duration="0.33" note="C2"/>


### Techno Halo Score Sheet

Below is a 1.5 minute song using the effects and subtractive synthesis components. It is also held in this repository as 8BitHalo.score and you can download by pressing [here](https://gitlab.msu.edu/langerno/cse471-1/-/raw/main/8BitHalo.score?ref_type=heads&inline=false)
<details>
    <summary> Techno Halo Full Score Sheet </summary>

        <?xml version="1.0" encoding="utf-8"?>
        <score bpm="120" beatspermeasure="4">

        <!-- Treble Clef Melody -->

        <instrument instrument="SubtractiveSynth" wave="sawtooth" resonSettings="0.0098,1.0,1.0" resonBandwidthASDR="0.0,0.75,0.1,0.0" ASDRSettings="0.0,0.2,0.0,0.0" effectDist="0.0,0.5,0.0,0.5,0.0">
            <note measure="2" beat="1" duration="2" note="E3"/>
            <note measure="2" beat="3" duration="1" note="F#3"/>
            <note measure="2" beat="4" duration="1" note="G3"/>

            <note measure="3" beat="1" duration="1" note="F#3"/>
            <note measure="3" beat="2" duration="1" note="A3"/>
            <note measure="3" beat="3" duration="1" note="G3"/>
            <note measure="3" beat="4" duration="1" note="F#3"/>

            <note measure="4" beat="1" duration="0.5" note="F#3"/>
            <note measure="4" beat="1.5" duration="0.5" note="E3"/>
            <note measure="4" beat="2" duration="3" note="E3"/>

            <note measure="5" beat="2" duration="1" note="B3"/>
            <note measure="5" beat="3" duration="1" note="C#4"/>
            <note measure="5" beat="4" duration="1" note="D4"/>

            <note measure="6" beat="1" duration="1" note="D4"/>
            <note measure="6" beat="2" duration="1" note="D4"/>
            <note measure="6" beat="3" duration="1" note="C#4"/>
            <note measure="6" beat="4" duration="1" note="A3"/>

            <note measure="7" beat="1" duration="1" note="C#4"/>
            <note measure="7" beat="2" duration="3" note="B3"/>

            <note measure="8" beat="1" duration="1" note="C3"/>
            <note measure="8" beat="2" duration="1" note="D3"/>
            <note measure="8" beat="3" duration="1" note="E3"/>
            <note measure="8" beat="4" duration="1" note="G3"/>

            <note measure="9" beat="1" duration="1" note="A3"/>
            <note measure="9" beat="2" duration="3" note="F#3"/>

            <note measure="10" beat="1" duration="1" note="E3"/>
            <note measure="10" beat="2" duration="1" note="G3"/>
            <note measure="10" beat="3" duration="1" note="F#3"/>
            <note measure="10" beat="4" duration="1" note="E3"/>

            <note measure="11" beat="1" duration="1" note="F#3"/>
            <note measure="11" beat="2" duration="3" note="D3"/>

            <note measure="12" beat="1" duration="4" note="E3"/>

            <note measure="13" beat="1" duration="0.33" note="E2"/>
            <note measure="13" beat="1.33" duration="0.33" note="E2"/>
            <note measure="13" beat="1.66" duration="0.33" note="E2"/>
            <note measure="13" beat="2" duration="3" note="C#3"/>

            <note measure="14" beat="1" duration="0.33" note="E2"/>
            <note measure="14" beat="1.33" duration="0.33" note="E2"/>
            <note measure="14" beat="1.66" duration="0.33" note="E2"/>
            <note measure="14" beat="2" duration="3" note="D3"/>

            <note measure="15" beat="1" duration="0.33" note="E2"/>
            <note measure="15" beat="1.33" duration="0.33" note="E2"/>
            <note measure="15" beat="1.66" duration="0.33" note="E2"/>
            <note measure="15" beat="2" duration="3" note="E3"/>

            <note measure="16" beat="1" duration="0.33" note="E2"/>
            <note measure="16" beat="1.33" duration="0.33" note="E2"/>
            <note measure="16" beat="1.66" duration="0.33" note="E2"/>
            <note measure="16" beat="2" duration="1" note="F#3"/>
            <note measure="16" beat="3" duration="0.33" note="E2"/>
            <note measure="16" beat="3.33" duration="0.33" note="F#3"/>
            <note measure="16" beat="3.66" duration="0.33" note="E2"/>
            <note measure="16" beat="4" duration="0.33" note="F#3"/>
            <note measure="16" beat="4.33" duration="0.33" note="E3"/>
            <note measure="16" beat="4.66" duration="0.33" note="D3"/>

            <note measure="17" beat="1" duration="1" note="F#3"/>
            <note measure="17" beat="2" duration="3" note="F#3"/>

            <note measure="18" beat="1" duration="1" note="E3"/>
            <note measure="18" beat="2" duration="2" note="F#3"/>
            <note measure="18" beat="3" duration="1" note="F#3"/>
            <note measure="18" beat="3" duration="1" note="G3"/>

            <note measure="19" beat="1" duration="4" note="A3"/>

            <note measure="20" beat="1" duration="4" note="B3"/>
            <note measure="20" beat="1" duration="1" note="A3"/>
            <note measure="20" beat="2" duration="1" note="A3"/>
            <note measure="20" beat="3" duration="1" note="F#3"/>
            <note measure="20" beat="3.33" duration="0.33" note="A3"/>
            <note measure="20" beat="4" duration="0.33" note="G3"/>
            <note measure="20" beat="4" duration="0.33" note="A3"/>
            <note measure="20" beat="4.33" duration="0.33" note="G3"/>
            <note measure="20" beat="4.66" duration="0.33" note="F#3"/>

            <note measure="21" beat="1" duration="4" note="C#3"/>
            <note measure="21" beat="1" duration="4" note="G3"/>
            <note measure="21" beat="1" duration="3" note="A3"/>
            <note measure="21" beat="4" duration="1" note="A3"/>

            <note measure="22" beat="1" duration="2" note="D3"/>
            <note measure="22" beat="1" duration="4" note="G3"/>
            <note measure="22" beat="1" duration="4" note="A3"/>
            <note measure="22" beat="3" duration="1" note="E4"/>
            <note measure="22" beat="4" duration="1" note="F#4"/>

            <note measure="23" beat="1" duration="2" note="G4"/>
            <note measure="23" beat="1" duration="4" note="G3"/>
            <note measure="23" beat="1" duration="4" note="A3"/>
            <note measure="23" beat="3" duration="2" note="A4"/>

            <note measure="24" beat="1" duration="4" note="B4"/>

            <note measure="25" beat="1" duration="4" note="B4"/>

            <note measure="26" beat="1" duration="0.33" note="E2"/>
            <note measure="26" beat="1.33" duration="0.33" note="E2"/>
            <note measure="26" beat="1.66" duration="0.33" note="E2"/>
            <note measure="26" beat="2" duration="1" note="A3"/>
            <note measure="26" beat="3" duration="0.33" note="E2"/>
            <note measure="26" beat="3.33" duration="0.33" note="A3"/>
            <note measure="26" beat="3.66" duration="0.33" note="E2"/>
            <note measure="26" beat="4" duration="0.33" note="A3"/>
            <note measure="26" beat="4.33" duration="0.33" note="G3"/>
            <note measure="26" beat="4.66" duration="0.33" note="F#3"/>


            <note measure="27" beat="1" duration="0.33" note="E2"/>
            <note measure="27" beat="1.33" duration="0.33" note="E2"/>
            <note measure="27" beat="1.66" duration="0.33" note="E2"/>
            <note measure="27" beat="2" duration="1" note="E3"/>
            <note measure="27" beat="3" duration="0.33" note="E2"/>
            <note measure="27" beat="3.33" duration="0.33" note="E2"/>
            <note measure="27" beat="3.66" duration="0.33" note="E2"/>
            <note measure="27" beat="4" duration="1" note="G3"/>


            <note measure="28" beat="1" duration="0.33" note="E2"/>
            <note measure="28" beat="1.33" duration="0.33" note="E2"/>
            <note measure="28" beat="1.66" duration="0.33" note="E2"/>
            <note measure="28" beat="2" duration="1" note="A3"/>
            <note measure="28" beat="3" duration="0.33" note="E2"/>
            <note measure="28" beat="3.33" duration="0.33" note="A3"/>
            <note measure="28" beat="3.66" duration="0.33" note="E2"/>
            <note measure="28" beat="4" duration="0.33" note="A3"/>
            <note measure="28" beat="4.33" duration="0.33" note="G3"/>
            <note measure="28" beat="4.66" duration="0.33" note="F#3"/>


            <note measure="29" beat="1" duration="0.33" note="B3"/>
            <note measure="29" beat="2.33" duration="0.33" note="B3"/>
            <note measure="29" beat="2.66" duration="0.33" note="C#3"/>
            <note measure="29" beat="3" duration="1" note="D3"/>
            <note measure="29" beat="4.33" duration="0.33" note="B3"/>
            <note measure="29" beat="4.66" duration="0.33" note="D3"/>

            <note measure="30" beat="1" duration="0.33" note="C#3"/>
            <note measure="30" beat="1.33" duration="0.33" note="B3"/>
            <note measure="30" beat="1.66" duration="0.33" note="A3"/>
            <note measure="30" beat="2" duration="1" note="E2"/>
            <note measure="30" beat="2" duration="0.33" note="A3"/>
            <note measure="30" beat="3" duration="0.33" note="E2"/>
            <note measure="30" beat="3.33" duration="0.33" note="B3"/>
            <note measure="30" beat="3.66" duration="0.33" note="C#3"/>
            <note measure="30" beat="4" duration="0.33" note="B3"/>
            <note measure="30" beat="4.33" duration="0.33" note="A3"/>
            <note measure="30" beat="4.66" duration="0.33" note="E2"/>

            <note measure="31" beat="1" duration="1" note="B3"/>
            <note measure="31" beat="2.33" duration="0.33" note="C#4"/>
            <note measure="31" beat="2.66" duration="0.33" note="D4"/>
            <note measure="31" beat="3" duration="1" note="E4"/>
            <note measure="31" beat="4.33" duration="0.33" note="F#3"/>
            <note measure="31" beat="4.66" duration="0.33" note="G3"/>

            <note measure="32" beat="1" duration="0.33" note="A3"/>
            <note measure="32" beat="1.33" duration="0.33" note="D4"/>
            <note measure="32" beat="1.66" duration="0.33" note="C#4"/>
            <note measure="32" beat="2" duration="0.33" note="D4"/>
            <note measure="32" beat="2.33" duration="0.33" note="C#4"/>
            <note measure="32" beat="2.66" duration="0.33" note="B3"/>
            <note measure="32" beat="3" duration="0.33" note="E4"/>
            <note measure="32" beat="3.33" duration="0.33" note="D4"/>
            <note measure="32" beat="3.66" duration="0.33" note="C#4"/>
            <note measure="32" beat="4" duration="0.33" note="B3"/>
            <note measure="32" beat="4.33" duration="0.33" note="D4"/>
            <note measure="32" beat="4.66" duration="0.33" note="E4"/>

            <note measure="33" beat="1" duration="0.33" note="F#4"/>
            <note measure="33" beat="1.33" duration="0.33" note="D4"/>
            <note measure="33" beat="1.66" duration="0.33" note="B3"/>
            <note measure="33" beat="2" duration="1" note="A4"/>
            <note measure="33" beat="3" duration="0.33" note="A4"/>
            <note measure="33" beat="3.33" duration="0.33" note="A4"/>
            <note measure="33" beat="3.66" duration="0.33" note="F#4"/>
            <note measure="33" beat="4" duration="0.33" note="A4"/>
            <note measure="33" beat="4.33" duration="0.33" note="G4"/>
            <note measure="33" beat="4.66" duration="0.33" note="A4"/>

            <note measure="34" beat="1" duration="0.33" note="E4"/>
            <note measure="34" beat="1.33" duration="0.33" note="D4"/>
            <note measure="34" beat="1.66" duration="0.33" note="B3"/>
            <note measure="34" beat="2" duration="1" note="A4"/>
            <note measure="34" beat="3" duration="0.33" note="A4"/>
            <note measure="34" beat="3.33" duration="0.33" note="D4"/>
            <note measure="34" beat="3.66" duration="0.33" note="E4"/>
            <note measure="34" beat="4" duration="0.33" note="F#4"/>
            <note measure="34" beat="4.33" duration="0.33" note="G4"/>
            <note measure="34" beat="4.66" duration="0.33" note="A4"/>

            <note measure="35" beat="1" duration="1" note="E4"/>
            <note measure="35" beat="2.33" duration="0.33" note="D4"/>
            <note measure="35" beat="2.66" duration="0.33" note="E4"/>
            <note measure="35" beat="3" duration="1" note="F#4"/>
            <note measure="35" beat="4.33" duration="0.33" note="E4"/>
            <note measure="35" beat="4.66" duration="0.33" note="F#4"/>

            <note measure="36" beat="1" duration="0.33" note="G4"/>
            <note measure="36" beat="1.33" duration="0.33" note="F#4"/>
            <note measure="36" beat="1.66" duration="0.33" note="E4"/>
            <note measure="36" beat="2" duration="0.33" note="F#4"/>
            <note measure="36" beat="2.33" duration="0.33" note="E4"/>
            <note measure="36" beat="2.66" duration="0.33" note="D4"/>
            <note measure="36" beat="3" duration="1" note="C#4"/>
            <note measure="36" beat="3.66" duration="0.33" note="D4"/>
            <note measure="36" beat="4" duration="1" note="D4"/>
            <note measure="36" beat="4.66" duration="0.33" note="E4"/>

            <note measure="37" beat="1" duration="4" note="E5"/>
            <note measure="37" beat="1" duration="1" note="E4"/>
            <note measure="37" beat="2.33" duration="0.33" note="E4"/>
            <note measure="37" beat="2.66" duration="0.33" note="F#4"/>
            <note measure="37" beat="3" duration="1" note="G4"/>
            <note measure="37" beat="4.33" duration="0.33" note="E4"/>
            <note measure="37" beat="4.66" duration="0.33" note="G4"/>

            <note measure="38" beat="1" duration="0.33" note="F#4"/>
            <note measure="38" beat="1" duration="1" note="E5"/>
            <note measure="38" beat="1.33" duration="0.33" note="E4"/>
            <note measure="38" beat="1.66" duration="0.33" note="D4"/>
            <note measure="38" beat="2" duration="1" note="G4"/>
            <note measure="38" beat="2" duration="0.33" note="F#5"/>
            <note measure="38" beat="2.33" duration="0.33" note="E5"/>
            <note measure="38" beat="2.66" duration="0.33" note="D5"/>
            <note measure="38" beat="3" duration="0.33" note="D4"/>
            <note measure="38" beat="3.33" duration="0.33" note="C#4"/>
            <note measure="38" beat="3.66" duration="0.33" note="D4"/>
            <note measure="38" beat="4" duration="0.33" note="D4"/>
            <note measure="38" beat="4.33" duration="0.33" note="C#4"/>
            <note measure="38" beat="4.66" duration="0.33" note="D4"/>

            <note measure="39" beat="1.33" duration="0.33" note="D4"/>
            <note measure="39" beat="1.66" duration="0.33" note="G4"/>
            <note measure="39" beat="2.33" duration="0.33" note="D4"/>
            <note measure="39" beat="2.66" duration="0.33" note="C4"/>
            <note measure="39" beat="3" duration="0.33" note="G4"/>
            <note measure="39" beat="3.33" duration="0.33" note="D4"/>
            <note measure="39" beat="3.66" duration="0.33" note="F#4"/>
            <note measure="39" beat="4" duration="0.33" note="C4"/>
            <note measure="39" beat="4.33" duration="0.33" note="G4"/>
            <note measure="39" beat="4.66" duration="0.33" note="D4"/>

            <note measure="40" beat="1" duration="0.33" note="F4"/>
            <note measure="40" beat="1.33" duration="0.33" note="C4"/>
            <note measure="40" beat="1.66" duration="0.33" note="G4"/>
            <note measure="40" beat="2" duration="0.33" note="A4"/>
            <note measure="40" beat="2.33" duration="0.33" note="A#4"/>
            <note measure="40" beat="2.66" duration="0.33" note="C5"/>
            <note measure="40" beat="3" duration="0.33" note="A#4"/>
            <note measure="40" beat="3.33" duration="0.33" note="A4"/>
            <note measure="40" beat="3.66" duration="0.33" note="A4"/>
            <note measure="40" beat="4" duration="0.33" note="E4"/>
            <note measure="40" beat="4.33" duration="0.33" note="G4"/>
            <note measure="40" beat="4.66" duration="0.33" note="D4"/>

            <note measure="41" beat="1" duration="0.33" note="A4"/>
            <note measure="41" beat="1.33" duration="0.33" note="E4"/>
            <note measure="41" beat="1.66" duration="0.33" note="G4"/>
            <note measure="41" beat="2" duration="0.33" note="D4"/>
            <note measure="41" beat="2.33" duration="0.33" note="A4"/>
            <note measure="41" beat="2.66" duration="0.33" note="E4"/>
            <note measure="41" beat="3" duration="0.33" note="G4"/>
            <note measure="41" beat="3.33" duration="0.33" note="D4"/>
            <note measure="41" beat="3.66" duration="0.33" note="A4"/>
            <note measure="41" beat="4" duration="0.33" note="E4"/>
            <note measure="41" beat="4.33" duration="0.33" note="G4"/>
            <note measure="41" beat="4.66" duration="0.33" note="D4"/>

            <note measure="42" beat="1" duration="0.33" note="F#4"/>
            <note measure="42" beat="1.33" duration="0.33" note="G4"/>
            <note measure="42" beat="1.66" duration="0.33" note="F#4"/>
            <note measure="42" beat="2" duration="0.33" note="E4"/>
            <note measure="42" beat="2.33" duration="0.33" note="D4"/>
            <note measure="42" beat="2.66" duration="0.33" note="D4"/>
        </instrument>




        <!-- Bass cleff harmony -->

        <instrument instrument="SubtractiveSynth" wave="triangle" resonSettings="0.0098, 0.015, 8.0" resonFrequencyASDR="0.1,0.1,0.1,0.1" ASDRSettings="0.0,0.2,0.0,0.0" effectDist="0.1,0.3,0.0,0.0,0.2">
            <note measure="1" beat="1" duration="4" note="E1"/>

            <note measure="2" beat="1" duration="4" note="E2"/>
            <note measure="2" beat="1" duration="4" note="E1"/>

            <note measure="3" beat="1" duration="4" note="E2"/>
            <note measure="3" beat="1" duration="4" note="E1"/>

            <note measure="4" beat="1" duration="4" note="E2"/>
            <note measure="4" beat="1" duration="4" note="E1"/>

            <note measure="5" beat="1" duration="4" note="E2"/>
            <note measure="5" beat="1" duration="4" note="E1"/>

            <note measure="6" beat="1" duration="4" note="E2"/>
            <note measure="6" beat="1" duration="4" note="E1"/>

            <note measure="7" beat="1" duration="4" note="E2"/>
            <note measure="7" beat="1" duration="4" note="E1"/>

            <note measure="8" beat="1" duration="4" note="E2"/>
            <note measure="8" beat="1" duration="4" note="E1"/>

            <note measure="9" beat="1" duration="4" note="E2"/>
            <note measure="9" beat="1" duration="4" note="E1"/>

            <note measure="10" beat="1" duration="4" note="E2"/>
            <note measure="10" beat="1" duration="4" note="E1"/>

            <note measure="11" beat="1" duration="4" note="E2"/>
            <note measure="11" beat="1" duration="4" note="E1"/>

            <note measure="39" beat="1" duration="4" note="G3"/>
            
            <note measure="40" beat="1" duration="2" note="G3"/>
            <note measure="40" beat="4" duration="1" note="A4"/>

            <note measure="41" beat="1" duration="4" note="A4"/>

            <note measure="42" beat="1" duration="2" note="A4"/>
        </instrument>







        <!-- Bass Clef Melody -->

        <instrument instrument="SubtractiveSynth" wave="square" ASDRSettings="0.0,0.2,0.0,0.0" effectDist="0.0,0.5,0.0,0.5,0.0">
            <note measure="12" beat="1" duration="4" note="E2"/>
            <note measure="12" beat="1" duration="4" note="E1"/>

            <note measure="17" beat="1" duration="0.33" note="E2"/>
            <note measure="17" beat="1.33" duration="0.33" note="E2"/>
            <note measure="17" beat="1.66" duration="0.33" note="E2"/>

            <note measure="18" beat="1" duration="0.33" note="E2"/>
            <note measure="18" beat="1.33" duration="0.33" note="E2"/>
            <note measure="18" beat="1.66" duration="0.33" note="E2"/>

            <note measure="19" beat="1" duration="0.33" note="E2"/>
            <note measure="19" beat="1.33" duration="0.33" note="E2"/>
            <note measure="19" beat="1.66" duration="0.33" note="E2"/>
            <note measure="19" beat="2" duration="3" note="E3"/>

            <note measure="20" beat="1" duration="0.33" note="E2"/>
            <note measure="20" beat="1.33" duration="0.33" note="E2"/>
            <note measure="20" beat="1.66" duration="0.33" note="E2"/>
            <note measure="20" beat="3" duration="0.33" note="E2"/>
            <note measure="20" beat="3.66" duration="0.33" note="E2"/>

            <note measure="21" beat="1" duration="0.33" note="E2"/>
            <note measure="21" beat="1.33" duration="0.33" note="E2"/>
            <note measure="21" beat="1.66" duration="0.33" note="E2"/>
            <note measure="21" beat="2" duration="1" note="C#3"/>
            <note measure="21" beat="3.33" duration="0.33" note="E2"/>
            <note measure="21" beat="3.66" duration="0.33" note="E2"/>
            <note measure="21" beat="4" duration="0.33" note="E2"/>

            <note measure="22" beat="1" duration="0.33" note="E2"/>
            <note measure="22" beat="1.33" duration="0.33" note="E2"/>
            <note measure="22" beat="1.66" duration="0.33" note="E2"/>
            <note measure="22" beat="2" duration="1" note="D3"/>
            <note measure="22" beat="3" duration="0.33" note="E2"/>
            <note measure="22" beat="3.33" duration="0.33" note="E2"/>
            <note measure="22" beat="3.66" duration="0.33" note="E2"/>
            <note measure="22" beat="4" duration="1" note="D3"/>
            <note measure="22" beat="4" duration="0.33" note="C#3"/>

            <note measure="23" beat="1" duration="0.33" note="E2"/>
            <note measure="23" beat="1.33" duration="0.33" note="E2"/>
            <note measure="23" beat="1.66" duration="0.33" note="E2"/>
            <note measure="23" beat="2" duration="1" note="C#3"/>
            <note measure="23" beat="3" duration="0.33" note="E2"/>
            <note measure="23" beat="3.33" duration="0.33" note="E2"/>
            <note measure="23" beat="3.66" duration="0.33" note="E2"/>
            <note measure="23" beat="4" duration="1" note="D3"/>
            <note measure="23" beat="4" duration="0.33" note="C#3"/>

            <note measure="24" beat="1" duration="0.33" note="E2"/>
            <note measure="24" beat="1.33" duration="0.33" note="E2"/>
            <note measure="24" beat="1.66" duration="0.33" note="E2"/>
            <note measure="24" beat="2" duration="1" note="D3"/>
            <note measure="24" beat="3" duration="0.33" note="E2"/>
            <note measure="24" beat="3.33" duration="0.33" note="D3"/>
            <note measure="24" beat="3.66" duration="0.33" note="E2"/>
            <note measure="24" beat="4" duration="0.33" note="D3"/>
            <note measure="24" beat="4.33" duration="0.33" note="C#3"/>
            <note measure="24" beat="4.66" duration="0.33" note="B3"/>

            <note measure="25" beat="1" duration="0.33" note="E2"/>
            <note measure="25" beat="1.33" duration="0.33" note="E2"/>
            <note measure="25" beat="1.66" duration="0.33" note="E2"/>
            <note measure="25" beat="2" duration="1" note="E3"/>
            <note measure="25" beat="3" duration="0.33" note="E2"/>
            <note measure="25" beat="3.33" duration="0.33" note="E2"/>
            <note measure="25" beat="3.66" duration="0.33" note="E2"/>
            <note measure="25" beat="4" duration="1" note="G3"/>

            <note measure="29" beat="1" duration="0.33" note="E2"/>
            <note measure="29" beat="1.33" duration="0.33" note="E2"/>
            <note measure="29" beat="1.66" duration="0.33" note="E2"/>
            <note measure="29" beat="2" duration="3" note="E3"/>

            <note measure="30" beat="1" duration="0.33" note="E2"/>
            <note measure="30" beat="1.33" duration="0.33" note="E2"/>
            <note measure="30" beat="1.66" duration="0.33" note="E2"/>
            <note measure="30" beat="2" duration="3" note="F#3"/>

            <note measure="31" beat="1" duration="0.33" note="E2"/>
            <note measure="31" beat="1.33" duration="0.33" note="E2"/>
            <note measure="31" beat="1.66" duration="0.33" note="E2"/>
            <note measure="31" beat="2" duration="3" note="G3"/>

            <note measure="32" beat="1" duration="0.33" note="E2"/>
            <note measure="32" beat="1.33" duration="0.33" note="E2"/>
            <note measure="32" beat="1.66" duration="0.33" note="E2"/>
            <note measure="32" beat="2" duration="1" note="A4"/>
            <note measure="32" beat="3" duration="0.33" note="E2"/>
            <note measure="32" beat="3.33" duration="0.33" note="A4"/>
            <note measure="32" beat="3.66" duration="0.33" note="E2"/>
            <note measure="32" beat="4" duration="0.33" note="A4"/>
            <note measure="32" beat="4.33" duration="0.33" note="G4"/>
            <note measure="32" beat="4.66" duration="0.33" note="F#4"/>

            <note measure="33" beat="1" duration="0.33" note="E2"/>
            <note measure="33" beat="1.33" duration="0.33" note="E2"/>
            <note measure="33" beat="1.66" duration="0.33" note="E2"/>
            <note measure="33" beat="2" duration="1" note="E4"/>
            <note measure="33" beat="3" duration="0.33" note="E2"/>
            <note measure="33" beat="3.33" duration="0.33" note="E2"/>
            <note measure="33" beat="3.66" duration="0.33" note="E2"/>
            <note measure="33" beat="4" duration="1" note="F#4"/>

            <note measure="34" beat="1" duration="0.33" note="E2"/>
            <note measure="34" beat="1.33" duration="0.33" note="E2"/>
            <note measure="34" beat="1.66" duration="0.33" note="E2"/>
            <note measure="34" beat="2" duration="1" note="G4"/>
            <note measure="34" beat="3" duration="0.33" note="E2"/>
            <note measure="34" beat="3.33" duration="0.33" note="E2"/>
            <note measure="34" beat="3.66" duration="0.33" note="E2"/>
            <note measure="34" beat="4" duration="1" note="A4"/>

            <note measure="35" beat="1" duration="0.33" note="E2"/>
            <note measure="35" beat="1.33" duration="0.33" note="E2"/>
            <note measure="35" beat="1.66" duration="0.33" note="E2"/>
            <note measure="35" beat="2" duration="1" note="E3"/>
            <note measure="35" beat="3" duration="0.33" note="E2"/>
            <note measure="35" beat="3.33" duration="0.33" note="E2"/>
            <note measure="35" beat="3.66" duration="0.33" note="E2"/>
            <note measure="35" beat="6" duration="1" note="F#3"/>


            <note measure="36" beat="1" duration="0.33" note="E2"/>
            <note measure="36" beat="1.33" duration="0.33" note="E2"/>
            <note measure="36" beat="1.66" duration="0.33" note="E2"/>
            <note measure="36" beat="2" duration="1" note="G4"/>
            <note measure="36" beat="3" duration="0.33" note="E2"/>
            <note measure="36" beat="3.33" duration="0.33" note="E2"/>
            <note measure="36" beat="3.66" duration="0.33" note="E2"/>
            <note measure="36" beat="4" duration="1" note="A4"/>

            <note measure="37" beat="1" duration="0.33" note="E2"/>
            <note measure="37" beat="1.33" duration="0.33" note="E2"/>
            <note measure="37" beat="1.66" duration="0.33" note="E2"/>
            <note measure="37" beat="2" duration="1" note="E3"/>
            <note measure="37" beat="2" duration="1" note="E5"/>
            <note measure="37" beat="3" duration="0.33" note="E2"/>
            <note measure="37" beat="3.33" duration="0.33" note="E2"/>
            <note measure="37" beat="3.66" duration="0.33" note="E2"/>
            <note measure="37" beat="4" duration="1" note="F#3"/>
            <note measure="37" beat="4" duration="1" note="F#4"/>

            <note measure="38" beat="1" duration="0.33" note="E2"/>
            <note measure="38" beat="1.33" duration="0.33" note="E2"/>
            <note measure="38" beat="1.66" duration="0.33" note="E2"/>
            <note measure="38" beat="2" duration="0.33" note="E4"/>
            <note measure="38" beat="2" duration="0.33" note="F#4"/>
            <note measure="38" beat="2.33" duration="0.33" note="D4"/>
            <note measure="38" beat="2.66" duration="0.33" note="C#3"/>
            <note measure="38" beat="3" duration="0.33" note="E2"/>
            <note measure="38" beat="3.33" duration="0.33" note="E2"/>
            <note measure="38" beat="3.66" duration="0.33" note="E2"/>
            <note measure="38" beat="4" duration="1" note="G4"/>
            <note measure="38" beat="4" duration="1" note="G5"/>
        </instrument>
        </score>
</details>

